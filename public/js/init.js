function isNumber(data) {
    data = data ? data : window.event;
    var e = data.which ? data.which : data.keyCode;
    return e > 31 && (48 > e || e > 57) ? !1 : !0
}

$(document).off("click", ".submit").on("click", ".submit", function() {
    var element = $(this);

    $(".alert-notification-failed").hide(), 
    $(".alert-notification-success").hide();

    $("#form").ajaxForm({
        beforeSend: function() {
            element.prop("disabled", !0), element.html('<i class="fa fa-spinner fa-spin"></i> please wait')
        },
        success: function(e) {
            element.prop("disabled", !1); 
            element.html('<i class="glyphicon glyphicon-floppy-disk"></i> Save');
            par = JSON.parse(e); 
            par.status ? (
                $(".alert-notification-success").show(),
                $(".notif-msg").html(par.response), 
                $(".alert").delay(2e3).fadeOut(500), 
                $(".error-msg").remove(), 
                $('input[type="text"], select').popover("destroy"), 
                $("body").animate({
                    scrollTop: $(".alert").offset().top - 130
                }, 500)
            ) 
            : 
            (
                $(".alert-notification-failed").show(), 
                $(".notif-msg").html(par.response), 
                $(".error-msg").remove(), 
                $('input[type="text"], select').popover("destroy")
            )
        },
        error: function(data) {
            $error = data.responseJSON;
            $('input[type="text"], select').popover("destroy");
            block = 0; 
            $(".error-msg").remove();

            $.each($error['errors'], function(key, value) {
                // console.log(e[0])
                var errmsg = value[0];
                if (msg = '<div class="error-msg err-' + key + '" ><i class="fa fa-exclamation-circle" style="color:rgb(255, 184, 0)"></i> ' + errmsg+ "</div>", $('input[name="' + key + '"], textarea[name="' + key + '"], select[name="' + key + '"]').before(msg).attr("data-content", errmsg), 0 == block) {
                    try {
                        $("html, body").animate({
                            scrollTop: $(".err-" + key).offset().top - 130
                        }, 500)
                    } catch (l) {
                        console.log("error")
                    }
                    block++
                }
            });

            element.prop("disabled", !1);
            element.html('<i class="glyphicon glyphicon-floppy-disk"></i> Save');
            $(".alert-notification-failed").show();
            $(".notif-msg").html("please fill all necessary field");
            $(".alert").delay(2e3).fadeOut(500);
        },
        always: function() {}
    }).submit()
})

$(document).on("click", ".edit", function() {
    action = $(this).data("action");
    path = base_url + action; 
    window.location.href = path
})

$(document).on("click", ".delete", function() {
    self = $(this);
    idArr = [];
    idArr.push($(this).data("id"));
    url = base_url + module + "/"+idArr;

    confirm("You are about to delete a record!") && $.ajax({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data:{data:idArr},
        url: url,
        type: 'DELETE',
        success: function(result) {
            "deleted" == $.trim(result) && self.parent().closest("tr").remove()
        }
    });
});



$(document).on("click", ".pagination a", function() {
    return $(".pagination li").removeClass("active"); 
    linkArr = $(this).attr("href").split("/"); 
    page = linkArr[linkArr.length - 1];
    $.ajax({
        type: "GET",
        url: base_url + module + "/datatable/" + page,
        data: {
            q: $(".search").val(),
            type: $("#type").val(),
            "selected[]": $(".selected").val(),
            date: $("#date").val(),
            week: $("#week").val(),
            role: $("#role").val()
        },
        success: function(data) {
            $(".sub-panel").html(data)
        }
    }), !1
});

var timer;
$(document).on("keyup", ".search", function() {
    $("input.search").addClass("searchSpinner");
     _filters = $("#form-filters").serialize();
    clearTimeout(timer); 
    timer = setTimeout(function() {
        $.ajax({
            type: "GET",
            url: base_url + module + "/datatable",
            data: {
                q: $(".search").val(),
                filters: _filters
            },
            success: function(data) {
                $(".sub-panel").html(data); 
                $("input.search").removeClass("searchSpinner");
            }
        })
    }, 200)
});

$(document).on("change", ".limit", function() {
    clearTimeout(timer), timer = setTimeout(function() {
        $.ajax({
            type: "GET",
            url: base_url + module + "/datatable/1",
            data: {
                q: $(".search").val(),
                limit: $(".limit").val()
            },
            success: function(data) {
                $(".sub-panel").html(data)
            }
        })
    }, 500)
});

$(document).on("click", ".clear-form", function() {
    $(".error-msg").remove(); 
    $("input, select, textarea").val(""); 
    $("input").iCheck("uncheck");
});

// cart
codeArray = [];
$(document).on("click", ".addToCart", function() {
    element = $(this);
    code = element.data('code');

    if(jQuery.inArray(code, codeArray )){
        codeArray.push(code);
    }
    loadCart();
})

$(document).on("click", ".removeToCart", function() {
    element = $(this);
    code = element.data('code');

    codeArray.splice($.inArray(code, codeArray),1);
    loadCart();
})

function loadCart(){
    $.ajax({
        type: "GET",
        url: base_url + module + "/cart",
        data: {
            codeArray:codeArray
        },
        success: function(data) {
            $('.cart-panel').html(data);
        }
    })
}
