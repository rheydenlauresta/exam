<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Auth;

class Product extends Model
{

	use SoftDeletes;

    protected $table = 'products';
    
    protected $fillable = [
        'name',
        'price',
        'quantity',
        'size',
        'code',
    ];
}
