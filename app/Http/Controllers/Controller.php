<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Input;
use Crypt;
use DB;
use Auth;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function table_columns($table){
        
        $res = array();
        $data = DB::select('SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = "'.env("DB_DATABASE").'" AND TABLE_NAME="'.$table.'"');
        foreach ($data as $key => $value) {
        	$notInArray = array('id','created_at','updated_at');
        	// echo 
            if(!in_array($value->column_name,$notInArray)){            	
                array_push($res,$value->column_name);
            }
        }
        return $res;

    }

    /*---------------------------
        CMS Basic CRUD
    ----------------------------*/
    public function get_records($limit,$q,$table,$model)
    {
        $cols = $this->table_columns($table);
        $query  =   $model->where(function($query) use($cols, $q, $table){

                            $query = $query->where(function($qry) use($q, $cols, $table){
                                foreach ($cols as $key => $value) {
                                    $qry->orWhere($table.'.'.$value,'like','%'.$q.'%');
                                }
                            });

                        });
        $query = $query->select($table.'.*');
        
        $Response = $query  ->  paginate($limit);


        return $Response;
    }

    public function get_store($data,$model)
    {
        $model = new $model($data);

        $res = $model->save();
        
        if($res){
            $response = json_encode(['status'=>true,'response'=>'Saving was successful!']);

        }else{
            $response = json_encode(['status'=>false,'response'=>'Saving Failed!']);

        }

        return $response;
    }

    public function get_update($data,$model)
    {
        $data['id'] = Crypt::decrypt($data['id']);

        $model  = new $model($data);

        $res = $model->find($data['id'])->update($data);

        if($res){
            // 
            $response = json_encode(['status'=>true,'response'=>'Saving was successful!']);

        }else{
            $response = json_encode(['status'=>false,'response'=>'Saving Failed!']);

        }

        return $response;
    }

    public function get_destroy($form_data)
    {
        $data = array();
        foreach ($form_data['data'] as $key => $value) {
            array_push($data, Crypt::decrypt($value));
        }

        $res = DB::table($this->table)
                        ->whereIn('id',$data)
                        ->update(['deleted_at'=>date('Y-m-d H:i:s')]);

        if($res){ echo 'deleted'; }

    }

    public function get_code($col,$table, $length){
        $token=null;
        do{
            $alph_numeric = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $string=null;

            for($i = 0; $i< $length; ++$i){
                $string .= $alph_numeric[rand(0,strlen($alph_numeric)-1)];
            }
            
            $token = DB::select('SELECT '.$col.' FROM '.$table.' WHERE BINARY '.$col.'  = "'.$string.'"');

        }while(count($token) == 1);

        return $string;
    }
}
