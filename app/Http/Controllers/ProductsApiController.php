<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Product;

class ProductsApiController extends Controller
{
    public function index()
    {
        return Product::all();
    }

    public function store(Request $request)
    {
        $data = Input::all();
        $data['code'] = $this->get_code("code","products",4);

        $product = Product::create($data);

        return response()->json($product, 200);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id)->update($request->all());

        return response()->json($product, 200);
    }

    public function delete($id)
    {
        $product = Product::find($id)->delete();
        // return $product;

        return response()->json($product, 200);
    }
}
