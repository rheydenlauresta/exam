<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductsRequest;
use Illuminate\Support\Facades\Input;

use Illuminate\Http\Request;

use DB;
use Auth;
use Crypt;
use App\Product;

class ProductsController extends Controller
{

    function __construct(){
        $this->module = 'products';
        $this->table = 'products';

        #title panels
        $this->title   = 'Products';
        $this->eloquentModel = new Product();
        $this->controller    = $this;

        $this->response =     array(
                                'controller'        =>  $this->controller,
                                'title'             =>  $this->title,
                                'module'            =>  $this->module,
                            );
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view($this->module.'.index',$this->response);
    }

    // get datatable content
    public function getDatatable()
    {

        $q = Input::get('q');
        $limit = Input::get('limit');
        if(empty($limit)){ $limit  =  10; } //default

        $content = $this->get_records($limit,$q,$this->table,$this->eloquentModel);

        $response =     array(  'data'      =>  $content,
                                'cols'      =>  $this->table_columns($this->table),
                                'module'    =>  $this->module );

        return view($this->module.'.datatable',$response);
    }

    // get cart
    public function cart()
    {

        try{

            $data = Input::all();

            $cart =   DB::table($this->table)
                                ->whereIn('code',$data['codeArray'])
                                ->get();
        }catch(\Exception $e){
            $cart = null;
        }

        $response =     array(  'data'      =>  $cart,
                                'total'     =>  0,
                                'cols'      =>  $this->table_columns($this->table),
                                'module'    =>  $this->module );

        return view($this->module.'.cart',$response);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->module.'.create',$this->response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductsRequest $request)
    {
        $data = Input::all();
        $data['code'] = $this->get_code("code","products",4);

        $res = $this->get_store($data,$this->eloquentModel);

        return $res;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try{

        $id = Crypt::decrypt($id);

        $result =   DB::table($this->table)
                            ->where('id',$id)
                            ->first();

        $this->response['result'] = $result;

            return view($this->module.'.edit',$this->response);
        
        }catch(\Exception $e){
            abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ProductsRequest $request, $id)
    {
        $data = Input::all();
        $data['id'] = $id;

        $res = $this->get_update($data,$this->eloquentModel);

        return $res;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Input::all();

        $res = $this->get_destroy($data);

        return $res;
    }
}
