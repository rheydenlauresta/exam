    <table class="table">
        <thead>
            <th>Name</th>
            <th>Code</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Size</th>
            <th></th>
            <th></th>
        </thead>
        <tbody>
            <?php foreach ($data as $key => $value):?>

            <tr id="<?=$value->id?>">
                <td><?=ucwords($value->name) ?></td>
                <td><?=ucwords($value->code) ?></td>
                <td><?=ucwords($value->price) ?></td>
                <td><?=ucwords($value->quantity) ?></td>
                <td><?=ucwords($value->size) ?></td>
                <td><a href="javascript:;" class="addToCart" data-code="{{$value->code}}">Add to Cart </a></td>
                <td class="action-buttons">
                    

                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Action <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url($module.'/'.Crypt::encrypt($value->id).'/edit') }}">Edit</a></li>
                            <li><a href="#" class="delete" data-id="{{ Crypt::encrypt($value->id) }}">Delete</a></li>
                        </ul>
                    </div>
                </td>
            </tr>
            <?php endforeach;?>
        </tbody>
    </table>

    <!-- <span><button class="btn btn-danger btn-xs delete"><i class="fa fa-trash"></i> &nbsp;Remove</button></span> -->
    <span> {!! $data->render() !!}</span>



@section('js-logic2')
<script>

</script>
@endsection
