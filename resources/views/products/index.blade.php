@extends('app')

@section('content')
<div class="container">
    <!-- page content -->
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12">
            
            <h3>{{$title}}</h3>
                
            <div class="row">
                <a href="{{ URL::to($module.'/create') }}" class="btn btn-app" style="float:left"> <i class="fa fa-plus"></i> New</a>
                <div class="input-group search-wrapper">
                    <span class="input-group-btn">
                        <button class="btn btn-app  search-btn" type="button"><i class="fa fa-search"></i> Search</button>
                    </span>
                    <input type="text" style="" class="form-control search" placeholder="Enter keyword">
                </div>
            </div>

            <div class="sub-panel">
                {!! $controller->getDatatable() !!}
            </div>

        </div>

        <div class="col-md-3 col-sm-12 col-xs-12">

            <h3>Cart</h3>

            <div class="cart-panel">

            </div>
        </div>

    </div>
    <br />
    <!-- /page content -->
</div>
@endsection

@section('javascript')
<script type="text/javascript">
loadCart()
    
</script>
@endsection

