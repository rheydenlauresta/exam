<table class="table">
    <thead>
        <th>Name</th>
        <th>Code</th>
        <th>Quantity</th>
        <th>Size</th>
        <th>Price</th>
        <th></th>
    </thead>
    <tbody>
		@if($data != null)
	        <?php foreach ($data as $key => $value):?>
		        <?php $total = $value->price + $total; ?>
		        <tr id="<?=$value->id?>">
		            <td><?=ucwords($value->name) ?></td>
		            <td><?=ucwords($value->code) ?></td>
		            <td><?=ucwords($value->quantity) ?></td>
		            <td><?=ucwords($value->size) ?></td>
		            <td><?=ucwords($value->price) ?></td>
		            <td><a href="javascript:;" class="removeToCart" data-code="{{$value->code}}"><i class="fa fa-close"></i></a></td>

		        </tr>
	        <?php endforeach;?>
	    @else
	    	<td colspan="6"><center><h3>Empty Cart</h3></center></td>
		@endif
		<tr>
            <td></td>
            <td></td>
            <td></td>
            <td>Total: </td>
            <td>{{$total}}</td>

        </tr>
    </tbody>
</table>
