@extends('app')

@section('content')
<div class="container ">
    <div class="row">
        <div class="col-md-9 col-sm-12 col-xs-12">

            <h3>{{ $title }}</h3>

            <div class="row">
                <a href="{{ url($module) }}" class="btn btn-app"><i class="fa fa-long-arrow-left"></i> Return</a>
                <a class="btn btn-app submit"><i class="glyphicon glyphicon-floppy-disk"></i> Save </a>
                <a class="btn btn-app clear-form"><i class="glyphicon glyphicon-erase"></i> Clear </a>
            </div>

            <form method="POST" action="{{ url('/').'/'.$module}}" onsubmit="return false" id="form">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <section>
                    <div class="row">
                        <div class="col-sm-6 col-md-6">
                            <div class="form-group">
                                <label>Name</label>
                                <input type='text' name='name'  class='form-control'>
                            </div>

                            <div class="form-group">
                                <label>Price</label>
                                <input type='text' name='price'  class='form-control'>
                            </div>

                            <div class="form-group">
                                <label>Quantity</label>
                                <input type='text' name='quantity'  class='form-control'>
                            </div>

                            <div class="form-group">
                                <label>Size</label>
                                <input type='text' name='size'  class='form-control'>
                            </div>
                        </div>

                    </div>
                </section>
            </form>

        </div>
    </div>
</div>
@endsection

@section('js-logic2')
<script>

</script>
@endsection