<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title></title>

        <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/jquery-ui.theme.min.css') }}">
        <link rel="stylesheet" href="{{ asset('fonts/font-awesome.min.css') }}">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <script src="{{ asset('js/jquery.min.js') }}"></script>
        <script src="{{ asset('js/bootstrap.min.js') }} "></script>
        <script src="{{ asset('js/jquery.form.js') }} "></script>
        <script src="{{ asset('js/jquery-ui.js') }} "></script>
        <script src="{{ asset('js/init.js') }} "></script>

    </head>
    <body >

        <script type="text/javascript">
            base_url = "<?php echo URL::to('/'); ?>";
            module = "<?php echo '/'.@$module; ?>";
            var csrfToken = $('[name="csrf_token"]').attr('content');

            setInterval(refreshToken, 3600000); // 1 hour 

            function refreshToken(){
                $.get('refresh-csrf').done(function(data){
                    csrfToken = data; // the new token
                });
            }

            setInterval(refreshToken, 3600000); // 1 hour 
        </script>

        <div class="alert alert-success alert-notification-success" style="position:fixed">
            <span class="glyphicon glyphicon-ok"></span> <em class="notif-msg"> Saving Successful</em>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div class="alert alert-danger alert-notification-failed" style="position:fixed">
            <span class="glyphicon glyphicon-remove"></span> <em class="notif-msg"> Saving Failed!</em>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>

        <div>
            @yield('content')
        </div>

        @yield('javascript')

    </body>
</html>
