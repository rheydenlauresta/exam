<?php

use Illuminate\Database\Seeder;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        DB::table('products')->insert([
            'name' => str_random(10),
            'price' => rand(1,10000),
            'quantity' => rand(1,100),
            'size' => rand(1,10),
            'code' => str_random(4),
            
        ]);
    }
}
