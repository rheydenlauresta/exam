<?php

use Faker\Generator as Faker;

$factory->define(App\Product::class, function (Faker $faker) {
    return [
        'name' => str_random(10),,
        'price' => str_random(10),,
        'quantity' => str_random(10),,
        'size' => str_random(10),,
        'code' => str_random(10),,
    ];
});
